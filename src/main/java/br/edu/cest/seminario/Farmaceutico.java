package br.edu.cest.seminario;

public interface Farmaceutico {
	public String getNome();
	public Long getCpf();
	public Integer getId();
	public String getEmail();
	public String getEndereco();
	public Long getPhone();
	public String getCargo();
	public int getDate();
	public int getMonth();
	public int getYeah();
	public String toString();

}//interface é um tipo especial de classe que implementa uma 
//abstração completa e contém apenas métodos abstratos.
