package br.edu.cest.seminario;

import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Principal {
	Scanner scanner = new Scanner(System.in);
	ArrayList<Farmaceutico> farmac = new ArrayList<Farmaceutico>();
	
	public boolean registra(String nome, 
			Long cpf, 
			Integer id, 
			String email, 
			String endereco, 
			Long phone, 
			String cargo, 
			int date, 
			int month, 
			int yeah) {
				
		boolean res = false;
		Farmaceutico f = new Funcionario(nome, 
				cpf,
				id,
				email,
				endereco,
				phone,
				cargo,
				date,
				month,
				yeah);
		
		
		res = farmac.add(f);
		
		return res;
	}
	
	public void registrar() {
		System.out.println("Cadastro de Farmaceutico");
		String nome;
		Long cpf = (long) 0;
		Integer id;
		String email;
		String endereco;
		Long phone=(long) 0;
		String cargo;
		int date;
		int month;
		int yeah;
		
		//método registrar
		
		System.out.print("Digite seu nome: ");
		nome = scanner.nextLine();
		
		System.out.print("Digite seu cpf: ");
		cpf = scanner.nextLong();
		scanner.nextLine();
		
		System.out.print("digite seu id(Use números distintos): ");
		id = scanner.nextInt();
		scanner.nextLine();
		
		System.out.print("digite seu e-mail: ");
		email = scanner.nextLine();
		
		System.out.print("Digite seu endereço: ");
		endereco = scanner.nextLine();
		
		System.out.print("Digite seu telefone: ");
		phone = scanner.nextLong();
		scanner.nextLine();
		
		System.out.print("Digite seu cargo: ");
		cargo = scanner.nextLine();
		
		System.out.print("Digite sua data de nascimento: ");
		date = scanner.nextInt();
		scanner.nextLine();
		
		System.out.print("Digite seu mês de nascimento: ");
		month = scanner.nextInt();
		scanner.nextLine();
		
		System.out.print("Digite seu ano de nascimento: ");
		yeah = scanner.nextInt();
		scanner.nextLine();

		registra(nome, cpf, id,email,endereco,phone,cargo,date,month,yeah);
	}
	
	public void consultar(Scanner scanner) throws CodigoIncorreto, NaoEncontrado {
		Integer i = 0;
		System.out.println("Digite o ID (somente número):");
		i = scanner.nextInt();
		Farmaceutico res = null;
		
		System.out.println("Pesquisando por:" + i);
		
		for (Farmaceutico f : farmac) {
			if (i.equals(f.getId())) {
				System.out.println("Cadastro encontrado");
				System.out.println("Nome:" + f.getNome());
				res = f;
			}
		}
		if (res == null) throw new NaoEncontrado();
	
	}
	
	public void listar() 
	{for (Farmaceutico f : farmac) 
	{
		System.out.println(f);
			
	}
}
	public void remove(int id) {
		boolean found = false;
		for (int i = 0; i < farmac.size(); i++) {
			if (farmac.get(i).getId() == id){
				farmac.remove(i);
				found = true;
				JOptionPane.showMessageDialog(null, "Elemento removido com sucesso!");
	            break; // Saia do loop assim que o elemento for encontrado e removido
			}
			}
			if (!found) {
		        JOptionPane.showMessageDialog(null, "Elemento com o ID especificado não encontrado.");
		    }
		}
	
	public void total() {
		
		System.out.println("Total de cadastros: " + farmac.size());
	}
	
}