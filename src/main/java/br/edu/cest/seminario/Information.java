package br.edu.cest.seminario;

public interface Information {
	
	public String getNome();
	public Integer getId();
	public int getData_val();
	public int getMes_val();
	public int getAno_val();
	public int getEstoque();
	public String getLab();
	public String getTipo();
	
}//métodos get que retornam os elementos