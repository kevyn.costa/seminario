package br.edu.cest.seminario;

public class NaoEncontrado extends Exception {
	@Override
	public String getMessage() {
		return "Item nao encontrado";
	}
}
