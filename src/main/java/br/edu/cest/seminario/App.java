package br.edu.cest.seminario;

import javax.swing.JOptionPane;
import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
	Principal p = new Principal(); //instancia do objeto
	Dados d = new Dados();//instancia do objeto
	
	//Instanciar uma classe é criar um novo objeto do mesmo tipo dessa classe.
	
	Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		App a = new App();
		a.menu();
}
	private void menu() {
		int cadOption = -1;
		int conMenu = -1;
		int listMenu = -1;
		int delMenu = -1;
		int qtdMenu = -1;
		
		do {
			System.out.println("Escolha:");
			System.out.println("[1] - Cadastrar");
			System.out.println("[2] - Consultar");
			System.out.println("[3] - Listar");
			System.out.println("[4] - Remover");
			System.out.println("[5] - Quantidade");
			System.out.println("[0] - Sair");
			int option = scanner.nextInt();
			switch (option) {// switch instrução para selecionar um dos muitos blocos de código a serem executados.
			case 1://switch é avaliada uma vez.
				//O valor da expressão é comparado com os valores de cada um case.
				//Se houver uma correspondência, o bloco de código associado é executado.
				do {
                    System.out.println("Escolha:");
                    System.out.println("[1] - Cadastrar Funcionário");
                    System.out.println("[2] - Cadastrar Produto");
                    System.out.println("[0] - Voltar ao menu principal");
                    cadOption = scanner.nextInt();
                    switch (cadOption) {
                        case 1:
                            p.registrar();
                            break;
                        case 2:
                            d.cadastrar();
                            break;
                        case 0:
                            break;
                        default:
                            System.out.println("Opção inválida. Por favor, escolha novamente.");
                            break;
                    }
                } while (cadOption != 0);
                break;
			case 2:
				do {
					System.out.println("Escolha:");
					System.out.println("[1] - Consultar Funcionário");
					System.out.println("[2] - Consular Produto");
					System.out.println("[0] - Voltar ao Menu principal");
					conMenu = scanner.nextInt();
					switch (conMenu) {
					case 1:
						try {//é chamado de bloco protegido porque, 
							//caso ocorra algum problema com os comandos dentro do bloco,
							//a execução desviará para os blocos catch correspondentes.
							p.consultar(scanner);
					
						} catch (InputMismatchException e) {//tratamento para caso for digitado id não existente
							JOptionPane.showMessageDialog(null, e);
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null, e);
						} finally {
							//System.out.println("Sempre execute");
						}
						break;
					case 2:
						try {
							d.consultar(scanner);
						} catch (InputMismatchException e) {
							JOptionPane.showMessageDialog(null, e);
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null, e);
						} finally {
							//System.out.println("Sempre execute");
						}
						break;
					case 0:
                        break;
						
					default:
						System.out.println("Opção inválida. Por favor, escolha novamente.");
						break;
					}
				}while(conMenu != 0);
					break;

				
			case 3:
				do {
					System.out.println("Escolha:");
					System.out.println("[1] - Listar Funcionário");
					System.out.println("[2] - Listar Produto");
					System.out.println("[3] - Quantidade no estoque");
					System.out.println("[0] - Voltar ao Menu principal");
					listMenu = scanner.nextInt();
					switch(listMenu) {
					case 1:					
						p.listar();//chamar método listar
						break;
					case 2:
						d.listar();
						break;
					case 3:
						d.listestoq();
						break;
					case 0:
						break;
					default:
						System.out.println("Opção inválida. Por favor, escolha novamente.");
						break;
					}
				}while(conMenu != 0);
					break;
				
			case 4:
				do {
					System.out.println("Escolha:");
					System.out.println("[1] - Remover Funcionário");
					System.out.println("[2] - Remover Produto");
					System.out.println("[0] - Voltar ao Menu principal");
					delMenu = scanner.nextInt();
					switch(delMenu) {
					case 1:
						Scanner scanner = new Scanner(System.in);
						System.out.println("Digite o ID do funcionário a ser removido");
						int idEmployee = scanner.nextInt();
						p.remove(idEmployee);//remover funciário
						break;
					case 2:
						Scanner sc = new Scanner(System.in);
					    System.out.println("Digite o ID do produto a ser removido:");
					    int id = sc.nextInt();
					    d.remove(id);
					    break;
					case 0:
						break;
					default:
						System.out.println("Opção inválida. Por favor, escolha novamente.");
						break;
					}
				}while(delMenu != 0);
					break;
				
			case 5:
				do {
					System.out.println("Escolha");
					System.out.println("[1] - Quantidade de cadastros realizados");
					System.out.println("[2] - Quantidade de produtos cadastrados");
					System.out.println("[0] - Voltar ao Menu principal");
					qtdMenu = scanner.nextInt();
					switch(qtdMenu) {
					case 1:
						p.total();//total de cadastros de funcionarios feitos
						break;
					case 2:
						d.total();//total de produtos cadastrados
						break;
					case 0:
						break;
					default:
						System.out.println("Opção inválida. Por favor, escolha novamente.");
						break;
				}
			}while(qtdMenu != 0);
				break;
				
			case 0:
				 System.out.println("Fechando o programa...");
	             System.exit(0);
			}
		} while (true);	

	}
	
}