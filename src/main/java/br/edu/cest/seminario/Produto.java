package br.edu.cest.seminario;

public class Produto extends Medicamentos implements Information {
	
	private String nome;
	private Integer id;
	private int data_val;
	private int mes_val;
	private int ano_val;
	private int estoque;
	//variáveis dos elementos que serão armazenados em um Arraylist
	
	

	public Produto(String nome, 
			Integer id, 
			int data_val, 
			int mes_val, 
			int ano_val, 
			int estoque,  
			String lab, 
			String tipo) {
		this.nome=nome;
		this.id=id;
		this.data_val=data_val;
		this.mes_val=mes_val;
		this.ano_val=ano_val;
		this.estoque=estoque;
		this.lab=lab;
		this.tipo=tipo;
		

	}//Construtor com parâmetros
	
	
	@Override
	public String toString() { //O uso dessas classes ocorre quando precisamos usar 
		//e manipular muitas strings de caracteres
		
		//As principais operações em a StringBuffersão os métodos appende insert, 
		//que são sobrecarregados para aceitar dados de qualquer tipo
		//O appendmétodo sempre adiciona esses caracteres no final do buffer
		
		StringBuffer sb= new StringBuffer();
		sb.append("\nNome: "+ nome);
		sb.append("\nId: "+ id);
		sb.append("\nData de validade: "+ data_val + 
				"/" + mes_val + "/" + ano_val);
		sb.append("\nEstoque: " + estoque);
		sb.append("\nLaboratório: " + lab);
		sb.append("\nTipo: "+ tipo);
		return sb.toString();//sobrescrever um método de uma classe pai.
		// é necessário para estender ou modificar a implementação de um método
		
	}
	@Override
	public String getNome() {
		return nome;
	}
	@Override
	public Integer getId() {
		return id;
	}
	@Override
	public int getData_val() {
		return data_val;
	}
	@Override
	public int getMes_val() {
		return mes_val;
	}
	
	@Override
	public int getAno_val() {
		return ano_val;
	}
	
	@Override
	public int getEstoque() {
		return estoque;
	}
	
	@Override
	public String getLab() {
		return lab;
	}
	@Override
	public String getTipo() {
		return tipo;
	}
	
		
	}
