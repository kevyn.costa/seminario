package br.edu.cest.seminario;

public class Funcionario implements Farmaceutico {
	private String nome;
	private Long cpf;
	private Integer id;
	private String email;
	private String endereco;
	private Long phone;
	private String cargo;
	private int date;
	private int month;
	private int yeah;
	
//os métodos devem ser implementados na classe que está 
//herdando as propriedades da interface.

	

	public Funcionario(String nome, 
		Long cpf, 
		Integer id, 
		String email, 
		String endereco, 
		Long phone, 
		String cargo, 
		int date,
		int month,
		int yeah
	) {
		this.nome=nome;
		this.cpf=cpf;
		this.id=id;
		this.email=email;
		this.endereco=endereco;
		this.phone=phone;
		this.cargo=cargo;
		this.date=date;
		this.month=month;
		this.yeah=yeah;
	}
	
	
	@Override
	public String toString() {
		StringBuffer sb= new StringBuffer();
		sb.append("\nNome: "+ nome);
		sb.append("\nCPF: "+ cpf);
		sb.append("\nId: " + id);
		sb.append("\nE-mail: "+ email);
		sb.append("\nEndereco: " + endereco);
		sb.append("\nTelefone: " + phone);
		sb.append("\nCargo: " + cargo);
		sb.append("\nNascimento: "+ date + "/" + month + "/" + yeah);
		return sb.toString();
	}
	@Override
	public String getNome() {
		return nome;
	}

	
	@Override
	public Long getCpf() {
		return cpf;
	}

	
	@Override
	public Integer getId() {
		return id;
	}

	
	@Override
	public String getEmail() {
		return email;
	}

	
	@Override
	public String getEndereco() {
		return endereco;
	}

	@Override
	public Long getPhone() {
		return phone;
	}

	
	@Override
	public String getCargo() {
		return cargo;
	}

	
	@Override
	public int getDate() {
		return date;
	}

	
	@Override
	public int getMonth() {
		return month;
	}

	
	@Override
	public int getYeah() {
		return yeah;
	}


}

