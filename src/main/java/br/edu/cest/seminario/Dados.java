package br.edu.cest.seminario;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Dados {
	Scanner scanner = new Scanner(System.in);
	ArrayList<Information> dados = new ArrayList<Information>();
	//cria um ArrayList 
	
	public boolean cadastra(String nome, 
			Integer id, 
			int data_val, 
			int mes_val, 
			int ano_val, 
			int estoque, 
			String lab,
			String tipo) {
		boolean res=false;
		Information inf = new Produto(nome, id, 
				data_val, 
				mes_val, 
				ano_val, 
				estoque, 
				lab, 
				tipo);
		
		res=dados.add(inf); //guardar os elementos em "dados"
		System.out.println("Cadastro realizado!");
		return res;
		//método para armazenar dados, no arralist genercs
		//"dados".
	}
	
	public void cadastrar() {
		System.out.println("Cadastro de produto");
		String nome;
		Integer id;
		int data_val;
		int mes_val;
		int ano_val;
		int estoque;
		String lab;
		String tipo;
		
		System.out.print("Digite o nome do produto: ");
		nome = scanner.nextLine();
		
		System.out.print("digite o id do produto: ");
		id = scanner.nextInt();
		scanner.nextLine();
		
		System.out.print("Digite a data de validade: ");
		data_val = scanner.nextInt();
		scanner.nextLine();
		
		System.out.print("Digite o mês de validade: ");
		mes_val = scanner.nextInt();
		scanner.nextLine();
		
		System.out.print("Digite o ano de validade: ");
		ano_val = scanner.nextInt();
		scanner.nextLine();
		
		System.out.print("Digite a quantidade em estoque: ");
		estoque = scanner.nextInt();
		scanner.nextLine();
		
		System.out.print("Digite o laboratório: ");
		lab = scanner.nextLine();
		
		System.out.println("Digite o tipo de produto: ");
		tipo=scanner.nextLine();
		
		cadastra(nome, 
				id, 
				data_val, 
				mes_val, 
				ano_val, 
				estoque, 
				lab, 
				tipo);
					
}
	
	public void listar() 
	{for (Information inf : dados) 
	{
		System.out.println(inf);
		
		//método para imprimir no console os elementos da lista "dados"
		
	
	}
	}
	
	public void consultar(Scanner scanner) throws CodigoIncorreto, NaoEncontrado {
		Integer in;
		System.out.println("Digite o ID (somente número):");
		in = scanner.nextInt();
		Information res = null;
		
		System.out.println("Pesquisando por:" + in);
		
		for (Information inf : dados) {
			if (in.equals(inf.getId())) {//equals é utilizado para comparações. 
				//A classe String e as classes Wrapper sobrescrevem equals() 
				//para garantir que dois objetos desses tipos, com o mesmo conteúdo, possam ser considerados iguais.
				//classes especiais que possuem métodos capazes de fazer conversões em variáveis primitivas e também 
				//de encapsular tipos primitivos para serem trabalhados como objetos
				System.out.println("Cadastro encontrado");
				System.out.println("Nome:" + inf.getNome());
				res = inf;
			}
		}
		if (res == null) throw new NaoEncontrado();
		
		//método para buscar elemento entrando com o id através do scanner.
	}
	
	public void remove(int id) {
	    boolean found = false;
	    for (int i = 0; i < dados.size(); i++) {
	        if (dados.get(i).getId() == id) { // Supondo que cada elemento tenha um método getId() que retorna o ID
	            dados.remove(i);
	            found = true;
	            JOptionPane.showMessageDialog(null, "Elemento removido com sucesso!");
	            break; // Saia do loop assim que o elemento for encontrado e removido
	        }
	    }
	    if (!found) {
	        JOptionPane.showMessageDialog(null, "Elemento com o ID especificado não encontrado.");
	    }
	}
	
	public void total() {

		System.out.println("Total de produtos cadastrados: " + dados.size());
	}
	
	public void listestoq()  {
		int total=0;
		
		for(Information inf : dados) {
			total+=inf.getEstoque();}	
			 
				System.out.println("Estoque: "+total);
				
			}	
	}
